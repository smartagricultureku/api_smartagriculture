module.exports = function(app, db) {

    // POST REQUESTS

    app.post('/ph', (req, res) => {
        console.log('Post pH request is received')
        res.send("Request received successfuly")
    });

    app.post('/temp', (req, res) => {
        console.log('Post temperature request is received')
        res.send("Request received successfuly")
    });

    app.post('/ec', (req, res) => {
        console.log('Post electrical conductivity request is received')
        res.send("Request received successfuly")
    });

    // GET REQUESTS

    app.get('/ph/latest', (req, res) => {
        console.log('Get request for getting latest pH value is received')
        res.send("Request received successfuly")
    });

    app.get('/temp/latest', (req, res) => {
        console.log('Get request for getting latest temperature value is received')
        res.send("Request received successfuly")
    });

    app.get('/ec/latest', (req, res) => {
        console.log('Get request for getting latest e.c value is received')
        res.send("Request received successfuly")
    });


    // post 1: send ph 
    // post 2: send temp
    // post 3: send e.c

};